package za.co.omi.quickquote.page;

import com.aventstack.extentreports.MediaEntityBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import za.co.omi.quickquote.utilities.Utilities;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

public class CoverDetail {

    private WebDriver driver;

    public static final Logger log4j = Logger.getLogger(CoverDetail.class.getName());

    @FindBys(@FindBy(xpath = "//*[@formcontrolname='name']"))
    private List<WebElement> nameOfVessel;

    @FindBy(how = How.XPATH, using = "//*[@data-placeholder='Sum insured of Vessel']")
    private WebElement sumInsuredOfVessel;

    @FindBy(how = How.XPATH, using = "//*[@data-placeholder='Make and Model Of Vessel']")
    private WebElement makeAndModelOfVessel;

    @FindBy(how = How.XPATH, using = "//*[@data-placeholder='Year vessel was Manufactured']")
    private WebElement yearVesselWasManufactured;

    /**
     * This has been removed from the Jet-Ski screen
     * @FindBys(@FindBy(xpath = "//*[@formcontrolname='material']"))
    private List<WebElement> material;

    @FindBy(how = How.XPATH, using = "//*[@formcontrolname='vesselType']")
    private WebElement vesselType;
    **/
    @FindBy(how = How.XPATH, using = "//*[@data-placeholder='Horse Power']")
    private WebElement horsePower;

    @FindBy(how = How.XPATH, using = "//*[@formcontrolname='speed' and @type='number']")
    private WebElement speed;

    @FindBy(how = How.XPATH, using = "//*[@formcontrolname='terrLimit']")
    private WebElement terrLimit;

    @FindBy(how = How.XPATH, using = "//*[@formcontrolname='hasAdditionalLiabilty']")
    private WebElement hasAdditionalLiabilty;

    @FindBy(how = How.XPATH, using = "//*[@formcontrolname='hasExtras']")
    private WebElement hasExtras;

    @FindBys(@FindBy(xpath = "//*[text()='Load Item']"))
    //@FindBy(how = How.XPATH, using = "//*[text()='Load Item']")
    private List<WebElement> loadItem;

    @FindBy(how = How.XPATH, using = "//*[contains(text(),'Generate Quote')]")
    private WebElement generateQuote;

    // Jet Ski

    @FindBy(how = How.XPATH, using = "//*[@formcontrolname='sumInsured']")
    private WebElement sumInsured;

    @FindBy(how = How.XPATH, using = "//*[@formcontrolname='year']")
    private WebElement year;

    @FindBy(how = How.XPATH, using = "//*[@formcontrolname='makeAndModel']")
    private WebElement makeAndModel;

    @FindBy(how = How.XPATH, using = "//*[contains(text(),'Jet Ski')]")
    private WebElement jetSkiRadioButton;

    @FindBy(how = How.XPATH, using = "//*[contains(text(),'Hull')]")
    private WebElement hullRadioButton;

    @FindBy(how = How.XPATH, using = "//button[text()='Engine']")
    private WebElement engineButton;

    @FindBy(how = How.XPATH, using = "//*[@formcontrolname='material']")
    private WebElement jetSkiMaterial;

    @FindBy(how = How.XPATH, using = "//*[text()='Load Item']")
    private WebElement loadJetSkiItemButton;

    public CoverDetail(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        log4j.info("=============== Loading the cover details page ===============");
    }

    public void coverDetailsOption(String coverOption) throws InterruptedException, IOException {

        new WebDriverWait(driver, 120L).until(ExpectedConditions.elementToBeClickable(hullRadioButton));

        new WebDriverWait(driver, 120L).until(ExpectedConditions.jsReturnsValue("return document.readyState==\"complete\";"));
        log4j.info("Click | Waiting for Java script to load ");

        new WebDriverWait(driver, 120L).until(ExpectedConditions.elementToBeClickable(hullRadioButton));
        log4j.info("Wait | Waiting for element to be Clickable " + coverOption.toString());

        Thread.sleep(1500L);

        if (coverOption.equalsIgnoreCase("Hull")) {
            hullRadioButton.click();
            log4j.info("Click | Clicking on the option Hull on object " + coverOption.toString());
        } else {
            jetSkiRadioButton.click();
            log4j.info("Click | Clicking on the option Jet Ski on object " + coverOption.toString());
        }

        Utilities.extentTest.info("Providing cover details option ", MediaEntityBuilder.createScreenCaptureFromPath(Utilities.getScreenShot(this.driver)).build());
    }

    public void hullCoverDetails(String nameOfVessel, String sumInsuredOfVessel, String makeAndModelOfVessel, String yearVesselWasManufactured, String material, String vesselType,
                                 String horsePower, String speed, String terrLimit, String increaseLiabilty, String addExtras) throws IOException {

        if (!nameOfVessel.trim().equalsIgnoreCase("")) {
            this.nameOfVessel.clear();
            this.nameOfVessel.get(1).sendKeys(nameOfVessel);
            log4j.info("Enter | Entering the vessel name : " + nameOfVessel.toString());
        }

        if (!sumInsuredOfVessel.trim().equalsIgnoreCase("")) {
            this.sumInsuredOfVessel.clear();
            this.sumInsuredOfVessel.sendKeys(sumInsuredOfVessel);
            log4j.info("Enter | Entering sum insured of vessel : " + sumInsuredOfVessel);
        }

        if (!makeAndModelOfVessel.trim().equalsIgnoreCase("")) {
            this.makeAndModelOfVessel.clear();
            this.makeAndModelOfVessel.sendKeys(makeAndModelOfVessel);
            log4j.info("Enter | Entering make and model of vessel : " + makeAndModelOfVessel);
        }

        if (!yearVesselWasManufactured.trim().equalsIgnoreCase("")) {
            this.yearVesselWasManufactured.clear();
            this.yearVesselWasManufactured.sendKeys(yearVesselWasManufactured);
            log4j.info("Enter | Entering year vessel was manufactured : " + yearVesselWasManufactured);
        }

        if (!material.trim().equalsIgnoreCase("")) {
            driver.findElements(By.xpath("//*[@formcontrolname='material']")).get(0).click();
            this.driver.findElement(By.xpath("//*[contains(text(),'" + material + "')]")).click();
            log4j.info("Enter | Entering material from which vessel is constructed : " + yearVesselWasManufactured);
        }

        if (!vesselType.trim().equalsIgnoreCase("")) {
            driver.findElements(By.xpath("//*[@formcontrolname='vesselType']")).get(0).click();
            this.driver.findElement(By.xpath("//*[contains(text(),'" + vesselType + "')]")).click();
            log4j.info("Selecting | Selecting the vessel type : " + vesselType);
        }

        if (!horsePower.trim().equalsIgnoreCase("")) {
            driver.findElements(By.xpath("//*[@formcontrolname='horsePower']")).get(1).clear();
            driver.findElements(By.xpath("//*[@formcontrolname='horsePower']")).get(1).sendKeys(horsePower);
            log4j.info("Enter | Entering horse power : " + horsePower);
        }

        if (!speed.trim().equalsIgnoreCase("")) {
            driver.findElements(By.xpath("//*[@formcontrolname='speed']")).get(1).clear();
            driver.findElements(By.xpath("//*[@formcontrolname='speed']")).get(1).sendKeys(speed);
            log4j.info("Enter | Entering speed : " + speed);
        }

        if (!terrLimit.trim().equalsIgnoreCase("")) {
            driver.findElements(By.xpath("//*[@formcontrolname='terrLimit']")).get(1).click();
            this.driver.findElement(By.xpath("//*[contains(text(),'" + terrLimit + "')]")).click();
            log4j.info("Selecting | Selecting The territorial limit in which vessel will be used : " + terrLimit);
        }

        if (increaseLiabilty.trim().equalsIgnoreCase("Yes")) {
            driver.findElements(By.xpath("//*[@formcontrolname='hasAdditionalLiabilty']")).get(1).click();
            log4j.info("Selecting | Selecting The territorial limit in which vessel will be used : " + terrLimit);
        }

        if (addExtras.trim().equalsIgnoreCase("Yes")) {
            this.hasExtras.click();
            log4j.info("Click | Clicking on add extras :  " + addExtras);
        }

        Utilities.extentTest.info("Providing hull cover details ", MediaEntityBuilder.createScreenCaptureFromPath(Utilities.getScreenShot(this.driver)).build());
    }

    public void clickOnLoadHullItem(String coverType) throws InterruptedException {

        if (coverType.equalsIgnoreCase("Hull")) {
            this.loadItem.get(1).click();
            log4j.info("Click | Clicking on load item :  " + this.loadItem.get(1).toString());
        }
    }


    public void clickOnLoadJetSkiItemButton(String coverType) {
        if (coverType.equalsIgnoreCase("JetSki")) {
            loadJetSkiItemButton.click();
            log4j.info("Click | Clicking on load item :  " + this.loadJetSkiItemButton.toString());
        }

    }

    public void clickOnGenerateQuote() {
        this.generateQuote.click();
        log4j.info("Click | Clicking on load item :  " + this.loadItem.get(1).toString());
    }

    public void enterJetSkiCoverDetails(String makeAndModel, String sumInsured, String year, String jetSkiType, String material, String terrLimit, String horsepower, String speed, String increaseLiability) throws IOException {

        if (!makeAndModel.trim().equalsIgnoreCase("")) {
            this.makeAndModel.clear();
            this.makeAndModel.sendKeys(makeAndModel);
            log4j.info("Enter | entering make and model :  " + makeAndModel);
        }

        if (!sumInsured.trim().equalsIgnoreCase("")) {
            this.sumInsured.clear();
            this.sumInsured.sendKeys(sumInsured);
            log4j.info("Enter | entering sum insured :  " + sumInsured);
        }

        if (!year.trim().equalsIgnoreCase("")) {
            this.year.clear();
            this.year.sendKeys(year);
            log4j.info("Enter | entering year jet ski was manufactured :  " + year);
        }

        /**
         * This code has now been remove of the screen

        /*if (!jetSkiType.trim().equalsIgnoreCase("")) {
            this.vesselType.click();
            this.driver.findElement(By.xpath("//*[contains(text(),'" + jetSkiType + "')]")).click();
            log4j.info("Selecting | Selecting jet ski type " + jetSkiType);
        }

        if (!material.trim().equalsIgnoreCase("")) {
            this.jetSkiMaterial.click();
            this.driver.findElement(By.xpath("//*[contains(text(),'" + material + "')]")).click();
            log4j.info("Selecting | Selecting material from which Jet Ski is constructed " + material);
        }*/

        if (!terrLimit.trim().equalsIgnoreCase("")) {
            this.terrLimit.click();
            this.driver.findElement(By.xpath("//*[contains(text(),'" + terrLimit + "')]")).click();
            log4j.info("Selecting | Selecting Territorial limits in which Jet Ski will be used " + terrLimit);
        }

        if (!horsepower.trim().equalsIgnoreCase("")) {
            this.horsePower.clear();
            this.horsePower.sendKeys(horsepower);
            log4j.info("Enter | entering horse power " + horsepower);
        }

        if (!speed.trim().equalsIgnoreCase("")) {
            this.speed.clear();
            this.speed.sendKeys(speed);
            log4j.info("Enter | entering Max Speed of Jet Ski in knots " + speed);
        }

        if (increaseLiability.trim().equalsIgnoreCase("Yes")) {
            this.hasAdditionalLiabilty.click();
            log4j.info("Clicking | Would you like to increase liability limit to R 5,000,000? : " + increaseLiability);
        }
        Utilities.extentTest.info("Providing Jet Ski cover details ", MediaEntityBuilder.createScreenCaptureFromPath(Utilities.getScreenShot(this.driver)).build());
    }

    public void scrollToLoadButton(String hullOption) throws InterruptedException, IOException {

        if (hullOption.equalsIgnoreCase("Hull")) {
            WebElement element = driver.findElement(By.xpath("//button[text()='Engine']"));
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
            Thread.sleep(500);
        }
        Utilities.extentTest.info("Showing loaded item for Hull ", MediaEntityBuilder.createScreenCaptureFromPath(Utilities.getScreenShot(this.driver)).build());
    }

    public void scrollToGenerateQuoteForJetSki(String Option) throws IOException {

        if (Option.equalsIgnoreCase("JetSki")) {
            WebElement element = driver.findElement(By.xpath("//*[text()='Load Item']"));
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
        }
        Utilities.extentTest.info("Showing loaded item for JetSki ", MediaEntityBuilder.createScreenCaptureFromPath(Utilities.getScreenShot(this.driver)).build());
    }
}