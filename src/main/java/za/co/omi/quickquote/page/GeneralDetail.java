package za.co.omi.quickquote.page;

import com.aventstack.extentreports.MediaEntityBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.*;
import za.co.omi.quickquote.utilities.Utilities;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.logging.Logger;

public class GeneralDetail {

    private WebDriver driver;

    public static final Logger log4j = Logger.getLogger(GeneralDetail.class.getName());

    @FindBy( how = How.XPATH, using = "//*[@formcontrolname='brokerNumber']")
    private WebElement brokerNumber;

    @FindBy(how = How.XPATH, using = "//*[@fill='currentColor']")
    private WebElement coverStartDate;

    @FindBy(how = How.XPATH, using = "//*[@formcontrolname='usage']")
    private WebElement usage;

    @FindBy(how = How.XPATH, using = "//*[@formcontrolname='period']")
    private WebElement period;

    @FindBy(how = How.XPATH, using = "//*[contains(text(),'Has previous claims')]")
    private  WebElement hasPreviousClaims;

    @FindBy(how = How.XPATH, using = "//*[contains(text(),'No claims')]")
    private  WebElement noClaims;

    @FindBy(how = How.XPATH, using = "//*[@data-placeholder='Please provide information of all previous claims']")
    private  WebElement reasonForclaims;

    @FindBys(@FindBy(xpath="//*[text()='Next']"))
    private List<WebElement> nextButton;

    public GeneralDetail(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        log4j.info("=============== Launching the general details screen ===============");
    }

    public void enterGeneralDetails(String brokerNumber, String coverStartDate, String usage, String period, String previousClaim, String reasonForclaims) throws IOException {
        this.brokerNumber.clear();
        this.brokerNumber.sendKeys(brokerNumber);
        log4j.info("Enter | Entering broker number " + brokerNumber);

        this.coverStartDate.click();
        driver.findElement(By.xpath("//div[contains(text(),\"25\")]/parent::td")).click();
        log4j.info("Select | Selecting the date : " + brokerNumber);

        this.usage.click();
        driver.findElement(By.xpath("//*[contains(text(),'" + usage + "')]")).click();
        log4j.info("Select | Selecting the usage : " + usage);

        this.period.clear();
        this.period.sendKeys(period);
        log4j.info("Enter | Entering the period : " + period);

        if (previousClaim.equalsIgnoreCase("Yes")) {
            this.hasPreviousClaims.click();
            log4j.info("Clicking | Clicking on the option for previous claims : " + previousClaim);
            this.reasonForclaims.clear();
            this.reasonForclaims.sendKeys(reasonForclaims);
            log4j.info("Enter | Entering the reason for claim : " + reasonForclaims);
        } else {
            this.noClaims.click();
            log4j.info("Clicking | Clicking on the option for previous claims : " + previousClaim);
        }

        Utilities.extentTest.info("Providing general details ", MediaEntityBuilder.createScreenCaptureFromPath( Utilities.getScreenShot(this.driver)).build());
    }

    public void clickOnNextButton() {
        nextButton.get(1).click();
        log4j.info("Clicking | Clicking on the next button : " + nextButton.get(1).toString());
    }
}
