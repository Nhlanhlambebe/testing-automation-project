package za.co.omi.quickquote.page;

import com.aventstack.extentreports.MediaEntityBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import za.co.omi.quickquote.utilities.Utilities;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

public class InsuredDetail {

    private WebDriver driver;

    public static final Logger log4j = Logger.getLogger(InsuredDetail.class.getName());

    @FindBy(xpath = "//*[@data-placeholder='Insured Name']")
    private WebElement insuredName;

    @FindBy(xpath = "//*[@data-placeholder='ID/Company Registration Number ']")
    private WebElement ID_Company;

    @FindBy(how = How.XPATH, using = "//*[@data-placeholder='Description']")
    private WebElement description;

    @FindBy(how = How.XPATH, using = "//*[@data-placeholder='Email Address']")
    private WebElement emailAddress;

    @FindBy(how = How.XPATH, using = "//*[@data-placeholder='Contact Number']")
    private WebElement contactNumber;

    //@FindBy(how = How.XPATH, using = "//*[text()='Next' and @type='button']") Original
    //@FindBys(@FindBy(xpath = "//*[text()='Next' and @type='button']"))
    @FindBy(how = How.XPATH, using = "//*[text()='Next']")
    private WebElement nextButton;

    public InsuredDetail(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        log4j.info("=============== Insure details home page ===============");
    }

    public void enterInsuredDetail(String insuredName, String ID_Company, String description, String emailAddress, String contactNumber) throws IOException {

        if (!insuredName.equalsIgnoreCase("")) {
            this.insuredName.click();
            this.insuredName.sendKeys(insuredName);
            log4j.info("Enter | Entering the insured name : " + insuredName);
        }

        if (!ID_Company.equalsIgnoreCase("")) {
            this.ID_Company.click();
            this.ID_Company.sendKeys(insuredName);
            log4j.info("Enter | Entering ID/Company : " + ID_Company);
        }

        if (!description.equalsIgnoreCase("")) {
            this.description.click();
            this.description.sendKeys(description);
            log4j.info("Enter | Entering the description : " + description);
        }

        if (!emailAddress.equalsIgnoreCase("")) {
            this.emailAddress.click();
            this.emailAddress.sendKeys(emailAddress);
            log4j.info("Enter | Entering email address : " + emailAddress);
        }

        if (!contactNumber.equalsIgnoreCase("")) {
            this.contactNumber.click();
            this.contactNumber.sendKeys(contactNumber);
            log4j.info("Enter | Entering contact number : " + contactNumber);
        }

        Utilities.extentTest.info("Providing Insured detail ", MediaEntityBuilder.createScreenCaptureFromPath( Utilities.getScreenShot(this.driver)).build());
     }

    public void clickOnNextButton() {
        nextButton.click();
        log4j.info("Click | Clicking on the next button : " + nextButton.toString());

    }
}
