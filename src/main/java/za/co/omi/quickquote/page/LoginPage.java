package za.co.omi.quickquote.page;

import com.aventstack.extentreports.MediaEntityBuilder;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import za.co.omi.quickquote.utilities.Utilities;

import java.io.IOException;
import java.util.logging.Logger;

public class LoginPage {

    private WebDriver driver;

    public static final Logger log4j = Logger.getLogger(LoginPage.class.getName());

    @FindBy( id = "j_username")
    private WebElement userName;

    @FindBy( id = "j_password" )
    private WebElement password;

    @FindBy( id = "button_submit")
    private WebElement login;

    @FindBy( xpath = "//*[@class='card-header']")
    private WebElement loginText;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        log4j.info("================= Launching the Login page =====================");
    }

    public void enterLoginDetails(String username, String password) throws IOException {

        this.userName.clear();
        this.userName.sendKeys(username);
        log4j.info("Enter | Entering user name : " + username);

        this.password.clear();
        this.password.sendKeys(password);
        log4j.info("Enter | Entering password ");
        Utilities.extentTest.info("Launching the OMI quick quote login page", MediaEntityBuilder.createScreenCaptureFromPath( Utilities.getScreenShot(this.driver)).build());
    }

    public void clickOnLoginButton() {
        this.login.click();
        log4j.info("Click | Cliking on the login button ");
    }
}
