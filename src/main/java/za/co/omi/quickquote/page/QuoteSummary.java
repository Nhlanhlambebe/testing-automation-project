package za.co.omi.quickquote.page;

import com.aventstack.extentreports.MediaEntityBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import za.co.omi.quickquote.utilities.Utilities;

import java.io.IOException;

public class QuoteSummary {

    private WebDriver driver;

    @FindBy(xpath = "//*[contains(text(),'Insured Name:')]/following-sibling::div")
    private WebElement insuredName;

    @FindBy(xpath = "//*[contains(text(),'Contact No:')]/following-sibling::div")
    private WebElement contactNo;

    @FindBy(xpath = "//*[contains(text(),'Email Address:')]/following-sibling::div")
    private WebElement emailAddress;

    @FindBy(xpath = "//*[contains(text(),'Add New Quote')]")
    private WebElement addNewQuote;

    @FindBy(how = How.XPATH, using = "//*[text()='logout']/parent::a")
    private WebElement logoutButton;

    public QuoteSummary(WebDriver driver) throws InterruptedException {
        PageFactory.initElements(driver,this);
        this.driver = driver;
        Thread.sleep(2500L);
    }

    public void scrollToClientDetails() throws InterruptedException {
        new WebDriverWait(driver, 120L).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[text()='Client Details']")));
        WebElement element = driver.findElement(By.xpath("//*[text()='Premium Details']"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
        Thread.sleep(500);
    }

    public void validateClientDetails(String insuredName, String contactNumber, String emailAddress) throws IOException {
        new WebDriverWait(driver, 120L).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[contains(text(),'Insured Name:')]/following-sibling::div")));
        Assert.assertEquals(this.insuredName.getText().trim(), insuredName);
        Assert.assertEquals(this.contactNo.getText().trim(), contactNumber);
        Assert.assertEquals(this.emailAddress.getText().trim().toLowerCase(), emailAddress.toLowerCase());
        Utilities.extentTest.info("Validating company information ", MediaEntityBuilder.createScreenCaptureFromPath(Utilities.getScreenShot(this.driver)).build());
    }

    public void addNewQuote() {
        addNewQuote.click();
    }

    public void clickOnLogoutButton() {
        logoutButton.click();
    }
}
